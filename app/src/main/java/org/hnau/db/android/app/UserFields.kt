package org.hnau.db.android.app

import org.hnau.db.android.android
import org.hnau.db.android.string
import org.hnau.db.android.stringOrNull
import org.hnau.db.base.table.Column
import org.hnau.orm.Field
import org.hnau.orm.TypeAdapter


object UserFields {

    val login =
            Field(key = UserTable.columns.login, typeAdapter = TypeAdapter.android.string<Column>())

    val password =
            Field(key = UserTable.columns.password, typeAdapter = TypeAdapter.android.string<Column>())

    val description =
            Field(key = UserTable.columns.description, typeAdapter = TypeAdapter.android.stringOrNull<Column>())

}