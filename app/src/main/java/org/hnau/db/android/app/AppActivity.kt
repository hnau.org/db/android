package org.hnau.db.android.app

import android.app.Activity
import android.content.Context
import android.os.Bundle
import org.hnau.db.android.AndroidDBParametrizedSql
import org.hnau.db.android.AndroidExtractInfo
import org.hnau.db.android.count
import org.hnau.db.android.execute
import org.hnau.db.android.insert
import org.hnau.db.android.select
import org.hnau.db.android.update
import org.hnau.db.base.sql.ParametrizedSql
import org.hnau.db.base.table.Column
import org.hnau.db.base.table.alias
import org.hnau.orm.keyWithWriter
import org.hnau.orm.writer

class AppActivity : Activity() {

    companion object {
        private const val databaseName = "database"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        deleteDatabase(databaseName)
        openOrCreateDatabase(databaseName, Context.MODE_PRIVATE, null).use {
            it.run {
                execute {
                    """
                        CREATE TABLE ${UserTable.name} (
                                ${UserTable.columns.login} TEXT PRIMARY KEY,
                                ${UserTable.columns.password} TEXT NOT NULL,
                                ${UserTable.columns.description} TEXT
                        );
                    """.trimIndent()
                }

                insert(
                        table = UserTable,
                        values = listOf(
                                UserFields.login.keyWithWriter("hnau"),
                                UserFields.password.keyWithWriter("123456"),
                                UserFields.description.keyWithWriter(null)
                        )
                )

                update(
                        table = UserTable,
                        values = listOf(UserFields.password.keyWithWriter("new password")),
                        whereClause = { "${UserTable.columns.login} = ${inject(UserFields.login.writer("hnau"))}" }
                )

                val count = count(
                        table = UserTable,
                        wherePart = null
                )

                println(count)

                val user = select(
                        extractInfo = AndroidExtractInfo(
                                keys = listOf(
                                        UserFields.login.key,
                                        UserFields.password.key,
                                        UserFields.description.key
                                ),
                                extract = {
                                    Triple(
                                            UserFields.login.read(this),
                                            UserFields.password.read(this),
                                            UserFields.description.read(this)
                                    )
                                }
                        ),
                        fromPart = { "${UserTable.name}" },
                        wherePart = { "${UserTable.columns.login} = ${inject(UserFields.login.writer("hnau"))}" },
                        handleResult = { first() }
                )

                println(user)

            }
        }

    }
}
