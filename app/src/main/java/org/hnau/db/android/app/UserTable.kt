package org.hnau.db.android.app

import org.hnau.db.base.table.Table
import org.hnau.db.base.table.TableColumns


object UserTable : Table("t_user") {

    class Columns(table: Table) : TableColumns(table) {

        val login = column("login")
        val password = column("password")
        val description = column("description")

    }

    val columns = Columns(this)

}