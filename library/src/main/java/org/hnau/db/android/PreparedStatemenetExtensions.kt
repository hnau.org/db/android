package org.hnau.db.android

import android.content.ContentValues
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import org.hnau.base.extensions.sureNotNull
import org.hnau.db.android.AndroidDBValue
import org.hnau.db.base.sql.ParametrizedSql
import org.hnau.db.base.sql.ParamsInjector
import org.hnau.orm.ExtractInfo
import java.sql.Connection
import java.sql.PreparedStatement
import java.sql.ResultSet


inline fun AndroidDBParametrizedSql(
        build: ParamsInjector<AndroidDBValueReceiver>.() -> String
) = ParametrizedSql<AndroidDBValueReceiver>(
        build
)