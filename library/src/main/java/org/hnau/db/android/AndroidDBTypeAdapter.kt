package org.hnau.db.android

import org.hnau.db.android.AndroidDBValueProvider
import org.hnau.db.android.AndroidDBValueReceiver
import org.hnau.db.android.getInt
import org.hnau.db.android.getLong
import org.hnau.db.android.getShort
import org.hnau.db.android.setInt
import org.hnau.db.android.setLong
import org.hnau.db.android.setShort
import org.hnau.orm.TypeAdapter
import java.lang.Boolean.getBoolean


fun <K, T> AndroidDBTypeAdapter(
        read: AndroidDBValueProvider<K>.(K) -> T,
        write: AndroidDBValueReceiver.(K, T) -> Unit
) = TypeAdapter<K, T, AndroidDBValueReceiver, AndroidDBValueProvider<K>>(
        read = read,
        write = write
)

object AndroidDBTypeAdapters

val TypeAdapter.Companion.android
    get() = AndroidDBTypeAdapters

fun <K> AndroidDBTypeAdapters.short() = AndroidDBTypeAdapter<K, Short>(
        read = { key -> getShort<K>(key) },
        write = { _, value -> setShort(value) }
)

fun <K> AndroidDBTypeAdapters.int() = AndroidDBTypeAdapter<K, Int>(
        read = { key -> getInt<K>(key) },
        write = { _, value -> setInt(value) }
)

fun <K> AndroidDBTypeAdapters.long() = AndroidDBTypeAdapter<K, Long>(
        read = { key -> getLong<K>(key) },
        write = { _, value -> setLong(value) }
)

fun <K> AndroidDBTypeAdapters.float() = AndroidDBTypeAdapter<K, Float>(
        read = { key -> getFloat<K>(key) },
        write = { _, value -> setFloat(value) }
)

fun <K> AndroidDBTypeAdapters.double() = AndroidDBTypeAdapter<K, Double>(
        read = { key -> getDouble<K>(key) },
        write = { _, value -> setDouble(value) }
)

fun <K> AndroidDBTypeAdapters.string() = AndroidDBTypeAdapter<K, String>(
        read = { key -> getString<K>(key) },
        write = { _, value -> setString(value) }
)

fun <K> AndroidDBTypeAdapters.shortOrNull() = AndroidDBTypeAdapter<K, Short?>(
        read = { key -> getShortOrNull<K>(key) },
        write = { _, value -> setShortOrNull(value) }
)

fun <K> AndroidDBTypeAdapters.intOrNull() = AndroidDBTypeAdapter<K, Int?>(
        read = { key -> getIntOrNull<K>(key) },
        write = { _, value -> setIntOrNull(value) }
)

fun <K> AndroidDBTypeAdapters.longOrNull() = AndroidDBTypeAdapter<K, Long?>(
        read = { key -> getLongOrNull<K>(key) },
        write = { _, value -> setLongOrNull(value) }
)

fun <K> AndroidDBTypeAdapters.floatOrNull() = AndroidDBTypeAdapter<K, Float?>(
        read = { key -> getFloatOrNull<K>(key) },
        write = { _, value -> setFloatOrNull(value) }
)

fun <K> AndroidDBTypeAdapters.doubleOrNull() = AndroidDBTypeAdapter<K, Double?>(
        read = { key -> getDoubleOrNull<K>(key) },
        write = { _, value -> setDoubleOrNull(value) }
)

fun <K> AndroidDBTypeAdapters.stringOrNull() = AndroidDBTypeAdapter<K, String?>(
        read = { key -> getStringOrNull<K>(key) },
        write = { _, value -> setStringOrNull(value) }
)