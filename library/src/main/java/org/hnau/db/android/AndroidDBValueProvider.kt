package org.hnau.db.android

import android.database.Cursor
import org.hnau.base.extensions.boolean.ifFalse
import java.sql.ResultSet

interface AndroidDBValueProvider<K> {

    fun <T> get(
            key: K,
            getter: Cursor.(keyIndex: Int) -> T
    ): T

}

fun <K> AndroidDBValueProvider<K>.getShort(
        key: K
) = get(key) { keyIndex ->
    getShort(keyIndex)
}

fun <K> AndroidDBValueProvider<K>.getInt(
        key: K
) = get(key) { keyIndex ->
    getInt(keyIndex)
}

fun <K> AndroidDBValueProvider<K>.getLong(
        key: K
) = get(key) { keyIndex ->
    getLong(keyIndex)
}

fun <K> AndroidDBValueProvider<K>.getFloat(
        key: K
) = get(key) { keyIndex ->
    getFloat(keyIndex)
}

fun <K> AndroidDBValueProvider<K>.getDouble(
        key: K
) = get(key) { keyIndex ->
    getDouble(keyIndex)
}

fun <K> AndroidDBValueProvider<K>.getString(
        key: K
): String = get(key) { keyIndex ->
    getString(keyIndex)
}

inline fun <K, T> AndroidDBValueProvider<K>.getValueOrNull(
        key: K,
        crossinline getValue: Cursor.(keyIndex: Int) -> T
) = get(key) { keyIndex ->
    isNull(keyIndex).ifFalse { getValue(keyIndex) }
}

fun <K> AndroidDBValueProvider<K>.getShortOrNull(
        key: K
) = getValueOrNull(key) { keyIndex ->
    getShort(keyIndex)
}

fun <K> AndroidDBValueProvider<K>.getIntOrNull(
        key: K
) = getValueOrNull(key) { keyIndex ->
    getInt(keyIndex)
}

fun <K> AndroidDBValueProvider<K>.getLongOrNull(
        key: K
) = getValueOrNull(key) { keyIndex ->
    getLong(keyIndex)
}

fun <K> AndroidDBValueProvider<K>.getFloatOrNull(
        key: K
) = getValueOrNull(key) { keyIndex ->
    getFloat(keyIndex)
}

fun <K> AndroidDBValueProvider<K>.getDoubleOrNull(
        key: K
) = getValueOrNull(key) { keyIndex ->
    getDouble(keyIndex)
}

fun <K> AndroidDBValueProvider<K>.getStringOrNull(
        key: K
) = getValueOrNull(key) { keyIndex ->
    getString(keyIndex)
}