package org.hnau.db.android

import android.database.sqlite.SQLiteDatabase


enum class ConflictAlgorithm(
        val key: Int
) {

    rollback(SQLiteDatabase.CONFLICT_ROLLBACK),
    abort(SQLiteDatabase.CONFLICT_ABORT),
    fail(SQLiteDatabase.CONFLICT_FAIL),
    ignore(SQLiteDatabase.CONFLICT_IGNORE),
    none(SQLiteDatabase.CONFLICT_NONE),
    replace(SQLiteDatabase.CONFLICT_REPLACE);

    companion object {

        val default = none

    }

}