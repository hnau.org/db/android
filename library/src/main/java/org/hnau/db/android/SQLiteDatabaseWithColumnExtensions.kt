package org.hnau.db.android

import android.database.sqlite.SQLiteDatabase
import org.hnau.db.base.sql.ParametrizedSql
import org.hnau.db.base.sql.ParamsInjector
import org.hnau.db.base.sql.enumerate
import org.hnau.db.base.table.Column
import org.hnau.db.base.table.Table
import org.hnau.db.base.table.alias
import org.hnau.db.base.table.fullNameAsAlias
import org.hnau.orm.ExtractInfo

inline fun <K, T, R> SQLiteDatabase.select(
        extractInfo: ExtractInfo<K, T, AndroidDBValueProvider<K>>,
        keyToSqlSelectEnumerationList: K.() -> String,
        crossinline keyToCursorColumnName: K.() -> String,
        buildQuery: ParamsInjector<AndroidDBValueReceiver>.(selectItems: String) -> String,
        handleResult: Iterable<T>.() -> R
) = query(
        sqlBuilder = { buildQuery(extractInfo.keys.enumerate { it.keyToSqlSelectEnumerationList() }) },
        handleResult = { iterate(keyToCursorColumnName, extractInfo.extract, handleResult) }
)

inline fun <T, R> SQLiteDatabase.select(
        extractInfo: ExtractInfo<Column, T, AndroidDBValueProvider<Column>>,
        buildQuery: ParamsInjector<AndroidDBValueReceiver>.(selectItems: String) -> String,
        handleResult: Iterable<T>.() -> R
) = select(
        extractInfo = extractInfo,
        keyToSqlSelectEnumerationList = Column::fullNameAsAlias,
        keyToCursorColumnName = Column::alias,
        buildQuery = buildQuery,
        handleResult = handleResult
)

inline fun <T, R> SQLiteDatabase.select(
        extractInfo: ExtractInfo<Column, T, AndroidDBValueProvider<Column>>,
        wherePart: ParamsInjector<AndroidDBValueReceiver>.() -> String,
        fromPart: ParamsInjector<AndroidDBValueReceiver>.() -> String,
        handleResult: Iterable<T>.() -> R
) = select(
        extractInfo = extractInfo,
        buildQuery = { selectItems ->
            """
                SELECT $selectItems
                FROM ${fromPart()} 
                WHERE ${wherePart()}
            """.trimIndent()
        },
        handleResult = handleResult
)

fun SQLiteDatabase.count(
        table: Table,
        wherePart: (ParamsInjector<AndroidDBValueReceiver>.() -> String)? = null
) = count(
        fromPart = { "${table.name}" },
        wherePart = wherePart
)

fun SQLiteDatabase.insert(
        table: Table,
        values: Iterable<Pair<Column, AndroidDBValueReceiver.() -> Unit>>,
        conflictAlgorithm: ConflictAlgorithm = ConflictAlgorithm.default
) = insert(
        table = table.name,
        values = values,
        convertKeyToString = Column::selfName,
        conflictAlgorithm = conflictAlgorithm
)

fun SQLiteDatabase.update(
        table: Table,
        whereClause: ParamsInjector<AndroidDBValueReceiver>.() -> String,
        values: Iterable<Pair<Column, AndroidDBValueReceiver.() -> Unit>>,
        conflictAlgorithm: ConflictAlgorithm = ConflictAlgorithm.default
) = update(
        table = table.name,
        values = values,
        whereSqlBuilder = whereClause,
        convertKeyToString = Column::selfName,
        conflictAlgorithm = conflictAlgorithm
)