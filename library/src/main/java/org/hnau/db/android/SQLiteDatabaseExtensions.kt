package org.hnau.db.android

import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import org.hnau.base.extensions.checkNullable
import org.hnau.db.base.sql.ParamsInjector


inline fun <R> SQLiteDatabase.query(
        sqlBuilder: ParamsInjector<AndroidDBValueReceiver>.() -> String,
        handleResult: Cursor.() -> R
) = AndroidDBParametrizedSql(sqlBuilder).let { parametrizedSql ->
    this
            .rawQuery(
                    parametrizedSql.sql,
                    parametrizedSql.params.stringArray
            )
            .use(handleResult)
}

fun SQLiteDatabase.execute(
        sqlBuilder: ParamsInjector<AndroidDBValueReceiver>.() -> String
) = AndroidDBParametrizedSql(sqlBuilder).let { parametrizedSql ->
    execSQL(
            parametrizedSql.sql,
            parametrizedSql.params.rawArray
    )
}


inline fun <K> SQLiteDatabase.update(
        table: String,
        whereSqlBuilder: ParamsInjector<AndroidDBValueReceiver>.() -> String,
        values: Iterable<Pair<K, AndroidDBValueReceiver.() -> Unit>>,
        crossinline convertKeyToString: K.() -> String,
        conflictAlgorithm: ConflictAlgorithm = ConflictAlgorithm.default
) = AndroidDBParametrizedSql(whereSqlBuilder).let { parametrizedWhereSql ->
    updateWithOnConflict(
            table,
            values.toContentValues(convertKeyToString),
            parametrizedWhereSql.sql,
            parametrizedWhereSql.params.stringArray,
            conflictAlgorithm.key
    )
}

fun <K> SQLiteDatabase.delete(
        table: String,
        whereSqlBuilder: ParamsInjector<AndroidDBValueReceiver>.() -> String
) = AndroidDBParametrizedSql(whereSqlBuilder).let { parametrizedWhereSql ->
    delete(
            table,
            parametrizedWhereSql.sql,
            parametrizedWhereSql.params.stringArray
    )
}

inline fun <K> SQLiteDatabase.insert(
        table: String,
        values: Iterable<Pair<K, AndroidDBValueReceiver.() -> Unit>>,
        crossinline convertKeyToString: K.() -> String,
        conflictAlgorithm: ConflictAlgorithm = ConflictAlgorithm.default
) = insertWithOnConflict(
        table,
        null,
        values.toContentValues(convertKeyToString),
        conflictAlgorithm.key
)

inline fun SQLiteDatabase.count(
        crossinline fromPart: ParamsInjector<AndroidDBValueReceiver>.() -> String,
        noinline wherePart: (ParamsInjector<AndroidDBValueReceiver>.() -> String)? = null
) = select(
        extractInfo = AndroidExtractInfo(
                keys = listOf(Unit),
                extract = { getInt(Unit) }
        ),
        keyToSqlSelectEnumerationList = { "COUNT(*) as count" },
        keyToCursorColumnName = { "count" },
        buildQuery = { selectItems ->
            """
                SELECT $selectItems
                FROM ${fromPart()}${wherePart.checkNullable({ " WHERE $it" }, { "" })}
            """.trimIndent()
        },
        handleResult = Iterable<Int>::first
)
