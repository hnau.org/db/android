package org.hnau.db.android

import org.hnau.db.base.table.Column
import org.hnau.orm.ExtractInfo


fun <K, T> AndroidExtractInfo(
        keys: Iterable<K>,
        extract: AndroidDBValueProvider<K>.() -> T
) = ExtractInfo<K, T, AndroidDBValueProvider<K>>(
        keys = keys,
        extract = extract
)