package org.hnau.db.android

import android.database.Cursor
import org.hnau.base.data.associator.Associator
import org.hnau.base.data.associator.byHashMap
import org.hnau.base.data.associator.toAutoAssociator
import org.hnau.base.extensions.boolean.checkBoolean
import org.hnau.db.android.AndroidDBValueProvider
import org.hnau.orm.iterator.ExtractorCachingIterable
import org.hnau.orm.iterator.ExtractorOnceIterable
import org.hnau.orm.iterator.IterateInfo

inline fun <K> Cursor.toAndroidDBValueProvider(
        crossinline extractColumnName: (K) -> String
) = object : AndroidDBValueProvider<K> {

    private val columnsIndexesGetter = Associator
            .byHashMap<K, Int>()
            .toAutoAssociator { key -> getColumnIndexOrThrow(extractColumnName(key)) }


    override fun <T> get(
            key: K,
            getter: Cursor.(columnIndex: Int) -> T
    ) = getter(
            columnsIndexesGetter(key)
    )

}

@PublishedApi
internal inline fun <K, T> Cursor.toEntityExtractor(
        crossinline extractColumnName: (K) -> String,
        crossinline entityBuilder: AndroidDBValueProvider<K>.() -> T
): () -> T = toAndroidDBValueProvider(
        extractColumnName = extractColumnName
).run { { entityBuilder() } }

@PublishedApi
internal inline fun <K, T> Cursor.toIterateInfo(
        crossinline extractColumnName: (K) -> String,
        crossinline entityBuilder: AndroidDBValueProvider<K>.() -> T
) = IterateInfo(
        switchLine = ::moveToNext,
        close = ::close,
        extract = toEntityExtractor(extractColumnName, entityBuilder)
)

inline fun <K, T, R> Cursor.iterate(
        crossinline extractColumnName: (K) -> String,
        crossinline entityBuilder: AndroidDBValueProvider<K>.() -> T,
        handle: Iterable<T>.() -> R
) = ExtractorOnceIterable(
        iterateInfo = toIterateInfo(extractColumnName, entityBuilder)
).use(handle)

inline fun <K, T, R> Cursor.iterateCached(
        crossinline extractColumnName: (K) -> String,
        crossinline entityBuilder: AndroidDBValueProvider<K>.() -> T,
        handle: Iterable<T>.() -> R
) = ExtractorCachingIterable(
        iterateInfo = toIterateInfo(extractColumnName, entityBuilder)
).use(handle)