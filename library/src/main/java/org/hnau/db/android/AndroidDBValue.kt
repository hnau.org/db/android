package org.hnau.db.android

import android.content.ContentValues
import org.hnau.base.extensions.checkNullable


sealed class AndroidDBValue(
        val rawValue: Any?
) {

    open fun convertToString(): String = rawValue.toString()

    abstract fun insertIntoContentValue(
            key: String,
            contentValues: ContentValues
    )

    class short(
            val value: Short
    ) : AndroidDBValue(
            rawValue = value
    ) {

        override fun insertIntoContentValue(
                key: String,
                contentValues: ContentValues
        ) {
            contentValues.put(key, value)
        }

    }

    class int(
            val value: Int
    ) : AndroidDBValue(
            rawValue = value
    ) {

        override fun insertIntoContentValue(
                key: String,
                contentValues: ContentValues
        ) {
            contentValues.put(key, value)
        }

    }

    class long(
            val value: Long
    ) : AndroidDBValue(
            rawValue = value
    ) {

        override fun insertIntoContentValue(
                key: String,
                contentValues: ContentValues
        ) {
            contentValues.put(key, value)
        }

    }

    class float(
            val value: Float
    ) : AndroidDBValue(
            rawValue = value
    ) {

        override fun insertIntoContentValue(
                key: String,
                contentValues: ContentValues
        ) {
            contentValues.put(key, value)
        }

    }

    class double(
            val value: Double
    ) : AndroidDBValue(
            rawValue = value
    ) {

        override fun insertIntoContentValue(
                key: String,
                contentValues: ContentValues
        ) {
            contentValues.put(key, value)
        }

    }

    class string(
            val value: String
    ) : AndroidDBValue(
            rawValue = value
    ) {

        override fun insertIntoContentValue(
                key: String,
                contentValues: ContentValues
        ) {
            contentValues.put(key, value)
        }

    }

    abstract class ValueOrNull<T : Any>(
            val value: T?
    ) : AndroidDBValue(
            rawValue = value
    ) {

        protected abstract fun insertIntoContentValueIfNotNull(
                key: String,
                value: T,
                contentValues: ContentValues
        )

        override fun insertIntoContentValue(
                key: String,
                contentValues: ContentValues
        ) = value.checkNullable(
                ifNull = { contentValues.putNull(key) },
                ifNotNull = { existenceValue ->
                    insertIntoContentValueIfNotNull(
                            key = key,
                            value = existenceValue,
                            contentValues = contentValues
                    )
                }
        )

    }

    class shortOrNull(
            value: Short?
    ) : ValueOrNull<Short>(
            value = value
    ) {

        override fun insertIntoContentValueIfNotNull(
                key: String,
                value: Short,
                contentValues: ContentValues
        ) {
            contentValues.put(key, value)
        }

    }

    class intOrNull(
            value: Int?
    ) : ValueOrNull<Int>(
            value = value
    ) {

        override fun insertIntoContentValueIfNotNull(
                key: String,
                value: Int,
                contentValues: ContentValues
        ) {
            contentValues.put(key, value)
        }

    }

    class longOrNull(
            value: Long?
    ) : ValueOrNull<Long>(
            value = value
    ) {

        override fun insertIntoContentValueIfNotNull(
                key: String,
                value: Long,
                contentValues: ContentValues
        ) {
            contentValues.put(key, value)
        }

    }

    class floatOrNull(
            value: Float?
    ) : ValueOrNull<Float>(
            value = value
    ) {

        override fun insertIntoContentValueIfNotNull(
                key: String,
                value: Float,
                contentValues: ContentValues
        ) {
            contentValues.put(key, value)
        }

    }

    class doubleOrNull(
            value: Double?
    ) : ValueOrNull<Double>(
            value = value
    ) {

        override fun insertIntoContentValueIfNotNull(
                key: String,
                value: Double,
                contentValues: ContentValues
        ) {
            contentValues.put(key, value)
        }

    }

    class stringOrNull(
            value: String?
    ) : ValueOrNull<String>(
            value = value
    ) {

        override fun insertIntoContentValueIfNotNull(
                key: String,
                value: String,
                contentValues: ContentValues
        ) {
            contentValues.put(key, value)
        }

    }

}