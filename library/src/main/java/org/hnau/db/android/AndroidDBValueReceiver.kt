package org.hnau.db.android

import org.hnau.db.android.AndroidDBValue


typealias AndroidDBValueReceiver = (AndroidDBValue) -> Unit

fun AndroidDBValueReceiver.setShort(value: Short) =
        invoke(AndroidDBValue.short(value))

fun AndroidDBValueReceiver.setInt(value: Int) =
        invoke(AndroidDBValue.int(value))

fun AndroidDBValueReceiver.setLong(value: Long) =
        invoke(AndroidDBValue.long(value))

fun AndroidDBValueReceiver.setFloat(value: Float) =
        invoke(AndroidDBValue.float(value))

fun AndroidDBValueReceiver.setDouble(value: Double) =
        invoke(AndroidDBValue.double(value))

fun AndroidDBValueReceiver.setString(value: String) =
        invoke(AndroidDBValue.string(value))

fun AndroidDBValueReceiver.setShortOrNull(value: Short?) =
        invoke(AndroidDBValue.shortOrNull(value))

fun AndroidDBValueReceiver.setIntOrNull(value: Int?) =
        invoke(AndroidDBValue.intOrNull(value))

fun AndroidDBValueReceiver.setLongOrNull(value: Long?) =
        invoke(AndroidDBValue.longOrNull(value))

fun AndroidDBValueReceiver.setFloatOrNull(value: Float?) =
        invoke(AndroidDBValue.floatOrNull(value))

fun AndroidDBValueReceiver.setDoubleOrNull(value: Double?) =
        invoke(AndroidDBValue.doubleOrNull(value))

fun AndroidDBValueReceiver.setStringOrNull(value: String?) =
        invoke(AndroidDBValue.stringOrNull(value))