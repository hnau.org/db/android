package org.hnau.db.android

import android.content.ContentValues
import org.hnau.base.extensions.sureNotNull


val Iterable<AndroidDBValueReceiver.() -> Unit>.values
    get() = this.let { injectors ->
        ArrayList<AndroidDBValue>().apply {
            val receiver: AndroidDBValueReceiver = { value -> add(value) }
            injectors.forEach { inject -> inject(receiver) }
        }
    }

val Iterable<AndroidDBValueReceiver.() -> Unit>.stringArray
    get() = values.map(AndroidDBValue::convertToString).toTypedArray()

val Iterable<AndroidDBValueReceiver.() -> Unit>.rawArray
    get() = values.map(AndroidDBValue::rawValue).toTypedArray()

inline fun <K> Iterable<Pair<K, AndroidDBValueReceiver.() -> Unit>>.toContentValues(
        crossinline convertKeyToString: K.() -> String
) = ContentValues().apply {
    var currentKey: K? = null
    val receiver: AndroidDBValueReceiver = { value ->
        value.insertIntoContentValue(currentKey.sureNotNull().convertKeyToString(), this)
    }
    forEach { (key, injector) ->
        currentKey = key
        injector(receiver)
    }
}